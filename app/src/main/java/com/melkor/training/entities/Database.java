package com.melkor.training.entities;

import android.content.Intent;

/**
 * Created by francisco.crespo on 3/6/2018.
 */

public class Database {

    private Integer id;
    private String date;


    public Database(Integer id ,String date){
        this.id = id;
        this.date = date;

}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


}
