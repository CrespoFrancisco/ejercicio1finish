package com.melkor.training.entities;

/**
 * Created by francisco.crespo on 3/4/2018.
 */

import org.json.*;

import java.io.Serializable;
import java.util.*;


public class Addres implements Serializable{

    private String city;
    private Geo geo;
    private String street;
    private String suite;
    private String zipcode;

    public void setCity(String city){
        this.city = city;
    }
    public String getCity(){
        return this.city;
    }
    public void setGeo(Geo geo){
        this.geo = geo;
    }
    public Geo getGeo(){
        return this.geo;
    }
    public void setStreet(String street){
        this.street = street;
    }
    public String getStreet(){
        return this.street;
    }
    public void setSuite(String suite){
        this.suite = suite;
    }
    public String getSuite(){
        return this.suite;
    }
    public void setZipcode(String zipcode){
        this.zipcode = zipcode;
    }
    public String getZipcode(){
        return this.zipcode;
    }

    public String getFullAdress(){return "City: "+getCity()+ " Street: "+getStreet()+" Suite: "+getSuite() ;}

    /**
     * Instantiate the instance using the passed jsonObject to set the properties values
     */
    public Addres(JSONObject jsonObject){
        if(jsonObject == null){
            return;
        }
        city = jsonObject.optString("city");
        geo = new Geo(jsonObject.optJSONObject("geo"));
        street = jsonObject.optString("street");
        suite = jsonObject.optString("suite");
        zipcode = jsonObject.optString("zipcode");
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public JSONObject toJsonObject()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("city", city);
            jsonObject.put("geo", geo.toJsonObject());
            jsonObject.put("street", street);
            jsonObject.put("suite", suite);
            jsonObject.put("zipcode", zipcode);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}