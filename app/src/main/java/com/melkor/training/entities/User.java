package com.melkor.training.entities;

/**
 * Created by francisco.crespo on 3/4/2018.
 */

import org.json.*;

import java.io.Serializable;
import java.util.*;


public class User implements Serializable{

    private Addres address;
    private Company company;
    private String email;
    private int id;
    private String name;
    private String phone;
    private String username;
    private String website;

    public void setAddress(Addres address){
        this.address = address;
    }
    public Addres getAddress(){
        return this.address;
    }
    public void setCompany(Company company){
        this.company = company;
    }
    public Company getCompany(){
        return this.company;
    }
    public void setEmail(String email){
        this.email = email;
    }
    public String getEmail(){
        return this.email;
    }
    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setPhone(String phone){
        this.phone = phone;
    }
    public String getPhone(){
        return this.phone;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public String getUsername(){
        return this.username;
    }
    public void setWebsite(String website){
        this.website = website;
    }
    public String getWebsite(){
        return this.website;
    }


    /**
     * Instantiate the instance using the passed jsonObject to set the properties values
     */
    public User(JSONObject jsonObject){
        if(jsonObject == null){
            return;
        }
        address = new Addres(jsonObject.optJSONObject("address"));
        company = new Company(jsonObject.optJSONObject("company"));
        email = jsonObject.optString("email");
        id = jsonObject.optInt("id");
        name = jsonObject.optString("name");
        phone = jsonObject.optString("phone");
        username = jsonObject.optString("username");
        website = jsonObject.optString("website");
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public JSONObject toJsonObject()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("address", address.toJsonObject());
            jsonObject.put("company", company.toJsonObject());
            jsonObject.put("email", email);
            jsonObject.put("id", id);
            jsonObject.put("name", name);
            jsonObject.put("phone", phone);
            jsonObject.put("username", username);
            jsonObject.put("website", website);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}
