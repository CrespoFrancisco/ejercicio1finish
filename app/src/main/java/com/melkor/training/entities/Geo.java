package com.melkor.training.entities;

/**
 * Created by francisco.crespo on 3/4/2018.
 */

import org.json.*;

import java.io.Serializable;
import java.util.*;


public class Geo implements Serializable{

    private String lat;
    private String lng;

    public void setLat(String lat){
        this.lat = lat;
    }
    public String getLat(){
        return this.lat;
    }
    public void setLng(String lng){
        this.lng = lng;
    }
    public String getLng(){
        return this.lng;
    }


    /**
     * Instantiate the instance using the passed jsonObject to set the properties values
     */
    public Geo(JSONObject jsonObject){
        if(jsonObject == null){
            return;
        }
        lat = jsonObject.optString("lat");
        lng = jsonObject.optString("lng");
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public JSONObject toJsonObject()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("lat", lat);
            jsonObject.put("lng", lng);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}