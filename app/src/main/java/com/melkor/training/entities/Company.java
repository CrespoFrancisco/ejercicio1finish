package com.melkor.training.entities;

/**
 * Created by francisco.crespo on 3/4/2018.
 */

import org.json.*;

import java.io.Serializable;
import java.util.*;


public class Company implements Serializable{

    private String bs;
    private String catchPhrase;
    private String name;

    public void setBs(String bs){
        this.bs = bs;
    }
    public String getBs(){
        return this.bs;
    }
    public void setCatchPhrase(String catchPhrase){
        this.catchPhrase = catchPhrase;
    }
    public String getCatchPhrase(){
        return this.catchPhrase;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }


    /**
     * Instantiate the instance using the passed jsonObject to set the properties values
     */
    public Company(JSONObject jsonObject){
        if(jsonObject == null){
            return;
        }
        bs = jsonObject.optString("bs");
        catchPhrase = jsonObject.optString("catchPhrase");
        name = jsonObject.optString("name");
    }

    /**
     * Returns all the available property values in the form of JSONObject instance where the key is the approperiate json key and the value is the value of the corresponding field
     */
    public JSONObject toJsonObject()
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bs", bs);
            jsonObject.put("catchPhrase", catchPhrase);
            jsonObject.put("name", name);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonObject;
    }

}