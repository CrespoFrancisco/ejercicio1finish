package com.melkor.training.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.melkor.training.R;
import com.melkor.training.entities.User;

import java.util.List;

/**
 * Created by francisco.crespo on 3/5/2018.
 */

public class UserAdapter extends ArrayAdapter<User>
{
    Context mContext;
    List<User> userList;
    public UserAdapter(@NonNull Context context, int resource, @NonNull List<User> users) {
        super(context,0, users);
        mContext = context;
        userList = users;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(/*R.layout.list_view_user*/ android.R.layout.simple_expandable_list_item_2,parent,false);

        User currentUser = userList.get(position);


        //TextView name = (TextView) listItem.findViewById(R.id.nombre);
        TextView name = (TextView) listItem.findViewById(android.R.id.text1);
        name.setText(currentUser.getName());

        TextView email = (TextView) listItem.findViewById(android.R.id.text2);
        email.setText(currentUser.getEmail());

        return listItem;
    }

}
