package com.melkor.training.utils;

/**
 * Created by francisco.crespo on 3/6/2018.
 */

public class SQLConst {
    //contantes tabla timestamp
    public static final String TABLA_TIMESTAMP = "timestamp";
    public static final String CAMPO_ID = "id";
    public static final String CAMPO_DATE ="date";


    public static final String CREAR_TABLE_TIMESTAMP = "CREATE TABLE "+TABLA_TIMESTAMP+"("+CAMPO_ID+" INTEGER PRIMARY KEY,"
                                                                                            +CAMPO_DATE+" TEXT)";



}
