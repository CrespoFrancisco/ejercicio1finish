package com.melkor.training.utils;

/**
 * Created by francisco.crespo on 2/26/2018.
 */

public interface Callback {
        public void starting();
        public void completed(String res, int responseCode);
        public void completedWithErrors(Exception e);
        public void update();

}
