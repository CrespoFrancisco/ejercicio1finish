package com.melkor.training.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by francisco.crespo on 3/6/2018.
 */

public class SQLiteConnector extends SQLiteOpenHelper {


    // Contexto, nombre de la base,  factory null por ahora, version de la base
    // este contructor llama a oncreate automaticamente
    public SQLiteConnector(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override // todo crea las tablas aqui
    public void onCreate(SQLiteDatabase sql) {
        sql.execSQL(SQLConst.CREAR_TABLE_TIMESTAMP);
    }

    @Override // verifica si ya existe antes una version de nuestra base de datos
    public void onUpgrade(SQLiteDatabase sql, int oldVersion, int newVersion) {
        sql.execSQL("DROP TABLE IF EXISTS timestamp");
        sql.execSQL(SQLConst.CREAR_TABLE_TIMESTAMP);
    }
}
