package com.melkor.training;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.melkor.training.utils.SQLConst;
import com.melkor.training.utils.SQLiteConnector;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DB extends AppCompatActivity {

    Button btnDB;
    TextView cantRegistros;

    SimpleDateFormat dateFormat;
    SQLiteDatabase liteDatabase;

    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db);



        btnDB = (Button) findViewById(R.id.db_btn);
        cantRegistros = (TextView)findViewById(R.id.db_feedback);

        startConnection();
        Cursor c = liteDatabase.rawQuery("select * from "+SQLConst.TABLA_TIMESTAMP,null);
        cantRegistros.setText(String.valueOf(c.getCount()));
        liteDatabase.close();

        btnDB.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                startConnection();
                Cursor d = liteDatabase.rawQuery("select * from "+SQLConst.TABLA_TIMESTAMP,null);


                dateFormat = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy", Locale.getDefault());
                date = new Date();
                String fecha = dateFormat.format(date);
                System.out.println(fecha);


                ContentValues values = new ContentValues();


                values.put(SQLConst.CAMPO_DATE,fecha);

                Long idResultante = liteDatabase.insert(SQLConst.TABLA_TIMESTAMP,SQLConst.CAMPO_ID,values);
                Toast.makeText(getApplicationContext(),"id registrado"+ idResultante + " Hora "+ fecha ,Toast.LENGTH_SHORT).show();
                String count = String.valueOf(d.getCount());
                cantRegistros.setText(count);
                liteDatabase.close();




            }
        });






    }
    public void startConnection(){
        SQLiteConnector conn = new SQLiteConnector(DB.this,"db_timestamp",null,1);
        liteDatabase = conn.getWritableDatabase();





    }


}
