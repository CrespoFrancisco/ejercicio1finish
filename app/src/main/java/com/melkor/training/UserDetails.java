package com.melkor.training;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.melkor.training.entities.User;

import java.io.Serializable;

public class UserDetails extends AppCompatActivity {
    TextView company,email,id,name,phone,username,website,addres;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        addres = (TextView) findViewById(R.id.details_addres);
        company = (TextView)findViewById(R.id.details_company);
        email = (TextView)findViewById(R.id.details_email);
        id =(TextView)findViewById(R.id.details_id);
        name = (TextView)findViewById(R.id.details_name);
        phone = (TextView)findViewById(R.id.details_phone);
        username = (TextView)findViewById(R.id.details_username);
        website = (TextView)findViewById(R.id.details_website);

        Bundle objetoEnviado = getIntent().getExtras();
        User user=null;

        if(objetoEnviado!=null){
            user = (User) objetoEnviado.getSerializable("SU");



         // company.setText(user.getCompany().getName());
            company.setText(user.getCompany().getName());
            email.setText(user.getEmail());
            id.setText("id = "+String.valueOf(user.getId()));
            name.setText(user.getUsername());
            addres.setText(user.getAddress().getFullAdress());
            phone.setText("Phone = "+user.getPhone());
            username.setText("Username = "+user.getUsername());
            website.setText("Website = "+user.getWebsite());


        }





    }



}
