package com.melkor.training;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.melkor.training.entities.User;
import com.melkor.training.utils.AsyncConnector;
import com.melkor.training.utils.Callback;
import com.melkor.training.utils.UserAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class ConsumirServicio extends AppCompatActivity implements View.OnClickListener {

    ListView listaUsers;
    EditText latitud;
    EditText longitud;
    TextView resultado;
    Button datosGoogle;
    ProgressBar progressBar;
    String res;
    ArrayList<User> userList= new ArrayList<User>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumir_servicio);
        // ShowAlert("- Consumir el servicio de la siguiente URL: https://jsonplaceholder.typicode.com/users" +
        //        "\n\r - mostrar en un ListView los siguientes datos: username e email" +
        //       "\n\r - Al seleccionar un item de la lista ir a una pantalla de detalles donde se muestra el resto de la informacion");
        progressBar = (ProgressBar) findViewById(R.id.pbar);
        listaUsers =(ListView)findViewById(R.id.lv_cs);
        listaUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                User u2 = userList.get(pos);
                Intent intent = new Intent(ConsumirServicio.this,UserDetails.class);
                System.out.print(u2.getName());
                Bundle bundle = new Bundle();
                bundle.putSerializable("SU",u2);

                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        String content = "{}";
        String param = "";
        AsyncConnector ac = new AsyncConnector(AsyncConnector.USERLIST,param,content,new Callback(){

            @Override
            public void starting() {

            }

            @Override
            public void completed(String res, int responseCode) {
                //Intent intent = new Intent(ConsumirServicio.this,ConsumirServicio.class);
                //intent.putExtra("RES",res);
                setJson(res);
                progressBar.setVisibility(View.GONE);


            }

            @Override
            public void completedWithErrors(Exception e) {

            }

            @Override
            public void update() {

            }

        });
        ac.execute();




        //listado desde un array de xml



                //
        // datosGoogle.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {



    }

    public void setJson(String res){
        //Intent intent = getIntent();
        //this.res = intent.getExtras().getString("RES");
        String usersString,user;
        try {
            User u;
            String userAux;
            JSONArray jsonArray = new JSONArray(res);
            JSONObject jObject;

            for(int i = 0; i < jsonArray.length(); i++){
                jObject = jsonArray.getJSONObject(i);
                //System.out.println(jObject.toString());

                u = new User(jObject);

                userList.add(u);



                System.out.println(u.getName());
            }
            UserAdapter userAdapter = new  UserAdapter(this,0, userList);
            listaUsers.setAdapter(userAdapter);





        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.res = res;

    }



}
