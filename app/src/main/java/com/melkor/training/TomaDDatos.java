package com.melkor.training;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TomaDDatos extends AppCompatActivity {
    String valor;
    EditText txtUser;
    Button btnIngresar;
    TextView showText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //aca esta buscando en el r

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toma_de_datos);


        txtUser = (EditText)findViewById(R.id.txt_user);
        btnIngresar = (Button)findViewById(R.id.btn_ingresar);
        showText = (TextView) findViewById(R.id.text_muestra);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                valor = txtUser.getText().toString();

                showText.setText( valor);
                Toast.makeText(getBaseContext(),valor,Toast.LENGTH_SHORT).show();
            }
        });




    }
}
